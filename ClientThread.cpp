//---------------------------------------------------------------------------

#include <System.hpp>
#pragma hdrstop


#include "ClientThread.h"
#include "CryptoProSystem.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(&UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall ClientThread::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------
#define WAITING_DELAY 5
#define SLEEP_DURATION 300

__fastcall ClientThread::ClientThread(CryptoSystem * clientNetCrypt, TClientSocket * Client, TLabel * MainLabel, TOpenDialog * SendFileOpenDialog, bool CreateSuspended)
	: TThread(CreateSuspended)
{
	FreeOnTerminate = true;
	this->clientNetCrypt = clientNetCrypt;
	this->Client = Client;
	this->MainLabel = MainLabel;
	this->SendFileOpenDialog = SendFileOpenDialog;
	clientRead = new TEvent(NULL, true, false, "clientRead", false);
    clientSend = new TEvent(NULL, true, false, "clientSend", false);
}
//---------------------------------------------------------------------------
void __fastcall ClientThread::Execute()
{
	while (true)
	{
		if (clientRead->WaitFor(WAITING_DELAY) == wrSignaled)
		{

			BYTE * buffer = NULL;
			clientBlockSize = Client->Socket->ReceiveLength();
			buffer = new BYTE[clientBlockSize];
			clientRead->ResetEvent();
			Client->Socket->ReceiveBuf(buffer, clientBlockSize);
			AnsiString parseBuf = (char*)buffer;

			if(parseBuf.SubString(1, parseBuf.Pos("@") - 1) == "ConnectOK")//���� ������ ��������, �� ���� ������� ���� �����
			{
				WCHAR * srcFile = L"PublicKey.pub";
				TFileStream * myStream;
				myStream = new TFileStream(srcFile, fmOpenRead | fmShareDenyNone);
				Client->Socket->SendText("FileName@ClientPublicKey.pub@" + IntToStr(myStream->Size) + "@Key@");
				Sleep(SLEEP_DURATION);
				Client->Socket->SendStream(myStream);
				Sleep(SLEEP_DURATION * 2);
			}
			else if(parseBuf.SubString(1, parseBuf.Pos("@") - 1) == "FileName") //���� ����������� ������������ ����
			{
				clientCurrentFileSize = 0;
				parseBuf.Delete(1, parseBuf.Pos("@"));
				AnsiString dstFileNameAnsi = parseBuf.SubString(1, parseBuf.Pos("@") - 1);
				int size = dstFileNameAnsi.WideCharBufSize();
				WCHAR * dstFileName = new WCHAR[size];
				dstFileNameAnsi.WideChar(dstFileName, size);
				parseBuf.Delete(1, parseBuf.Pos("@"));
				clientFileSize = StrToInt(parseBuf.SubString(1, parseBuf.Pos("@") - 1));
				hClientDst = CreateFileW(                 //��������� ���� ����������
								dstFileName,
								GENERIC_WRITE,
								0,
								NULL,
								CREATE_ALWAYS,
								FILE_ATTRIBUTE_NORMAL,
								NULL);
				delete[] dstFileName;

			}
			else	//������ ��������� ����� �������
			{
				DWORD nWrote = 0;
				WriteFile(
					hClientDst,
					buffer,
					clientBlockSize,
					&nWrote,
					NULL);
				clientCurrentFileSize += clientBlockSize;
				if (clientCurrentFileSize >= clientFileSize)
				{
					CloseHandle(hClientDst);
					MainLabel->Font->Color = clGreen;
					MainLabel->Font->Style = TFontStyles() << fsBold;
					MainLabel->Caption = L"���������� �����������";
		//-----------------------------------------------------------------------------------------
		//                  ������������� �������������
		//            �������� ����: ServerPublicKey.pub(����� ����) � PublicKey.pub(���� ����)
					clientNetCrypt->Init(CryptoAlgorythms::MAGMA, CryptoAlgorythms::EXCHANGE_DH, L"ServerPublicKey.pub");
		//----------------------------------------------------------------------------------------
					DeleteFile(L"ServerPublicKey.pub");     //������� ��������� ���� ��������� ��������� ����� �������
				}
			}
			delete[] buffer;




		}
		if (clientSend->WaitFor(WAITING_DELAY) == wrSignaled)
		{
			WCHAR * srcFile = SendFileOpenDialog->FileName.c_str();
			String fileNameString = SendFileOpenDialog->FileName.c_str();
	//---------------------------------------------------------------------------
			//���� srcFile ��������� � ���� <srcFile>.enc
			fileNameString += L".enc";
			String srcFileCrypt = fileNameString.SubString(fileNameString.LastDelimiter( "\\" ) +
				1 , fileNameString.Length());
			clientNetCrypt->EncryptFile(srcFile, srcFileCrypt.c_str());
	//-----------------------------------------------------------------------------
			//�������� ������ ��� �������� �����
			TFileStream * myStream;
			myStream = new TFileStream(srcFileCrypt, fmOpenRead | fmShareDenyNone);
			//�������� ������������� ������ ���� FileName@��� �����.enc@������ �����@
			Client->Socket->SendText("FileName@" + srcFileCrypt + "@" + IntToStr(myStream->Size) + "@");
			Sleep(SLEEP_DURATION);
			//�������� �����
			Client->Socket->SendStream(myStream);  //����� �������������, ��� ������� �� �����
			MainLabel->Font->Color = clGreen;
			MainLabel->Font->Style = TFontStyles() << fsBold;
			MainLabel->Caption = "���� ���������: " + SendFileOpenDialog->FileName;
			DeleteFile(srcFileCrypt.c_str()); //������ �� �� �������
			clientSend->ResetEvent();
		}
	}

}
//---------------------------------------------------------------------------
