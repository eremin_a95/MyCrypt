//---------------------------------------------------------------------------

#pragma hdrstop

#include "CryptoContainer.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)



CryptoContainer::~CryptoContainer()
{
	if (containerName)
        delete[] containerName;
}

WCHAR * CryptoContainer::GetName() const
{
	WCHAR * name = new WCHAR[wcslen(containerName)+1];
	wcscpy(name,containerName);
	return name;
}

bool CryptoContainer::IsOpened() const
{
	return opened;
}
