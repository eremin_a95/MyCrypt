//---------------------------------------------------------------------------

#pragma hdrstop

#include "CryptoProContainer.h"

#pragma comment(lib, "crypt32.lib")
//#include "WinCryptEx.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
CryptoProContainer::CryptoProContainer(const WCHAR * name)
{
	containerName = NULL;
	opened = false;

	if (name)
	{
		this->Open(name);
		if (!IsOpened())
		{
            this->Create(name);
		}
	}

    hStore = 0;
}


CryptoProContainer::~CryptoProContainer()
{
	Close();
}


bool CryptoProContainer::Open(const WCHAR * name)
{
	//hStore = CertOpenSystemStore(0, name);
	hStore = CertOpenStore(
				 CERT_STORE_PROV_SYSTEM,
				 0,
				 NULL,
				 CERT_SYSTEM_STORE_CURRENT_USER,
				 name);
	if (hStore)
	{
        containerName = new WCHAR[wcslen(name)+1];
		wcscpy(containerName,name);
        opened = true;
		return true;
		//TODO: ��������� ���������
	}
	else
	{

		return false;
	}
}


bool CryptoProContainer::Create(const WCHAR * name)
{
	//TODO: ��������� �������� � ���������
}


void CryptoProContainer::Close()
{
    if (hStore)
		CertCloseStore(hStore, 0);
	hStore = 0;
}


bool CryptoProContainer::ExportPublicKey(const WCHAR * fileName)
{
    //TODO: �������������� �������� ���� �� ����������
}


bool CryptoProContainer::ExportPublicCert(const WCHAR * certName)
{
    //TODO: �������������� �������� ���� �� ���������� � ����������
}


bool CryptoProContainer::ImportPublicCert(const WCHAR * certName)
{
	/* TODO: return CertAddCertificateContextToStore(
			hStore,
			PCCERT_CONTEXT pCertContext,
			CERT_STORE_ADD_REPLACE_EXISTING,
			OPTIONAL PCCERT_CONTEXT * ppStoreContext); */
}


std::list<WCHAR *> CryptoProContainer::GetCertList() const
{
	std::list<WCHAR *> certList;
	PCCERT_CONTEXT  pCertContext = NULL;
	bool ended = false;
	while (!ended)
	{

		pCertContext = CertEnumCertificatesInStore(     //������ � crypto32.dll
							hStore,
							pCertContext);          	//pCertContext
		if (pCertContext == NULL)
		{
            ended = true;
		}
		else
		{
            certList.push_back((WCHAR *)pCertContext->pCertInfo->Subject.pbData);
        }

	}
    return certList;
}


BYTE * CryptoProContainer::GetPublicKey(const WCHAR * certName)
{
	//TODO: ����������� ������ ��������� ����� �� ����� �����������
}
