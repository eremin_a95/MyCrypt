//---------------------------------------------------------------------------
#ifndef CryptoContainerH
#define CryptoContainerH

#include <Windows.h>
#include <list>
//---------------------------------------------------------------------------
class CryptoContainer
{
protected:
	WCHAR * containerName;
    bool opened;

	//CryptoContainer(const WCHAR * name = NULL);

public:
	virtual ~CryptoContainer();

	WCHAR * GetName() const;

    bool IsOpened() const;

	virtual bool Open(const WCHAR * name) = 0;

	virtual bool Create(const WCHAR * name) = 0;

    virtual void Close() = 0;

	virtual bool ExportPublicKey(const WCHAR * fileName) = 0;

	virtual bool ExportPublicCert(const WCHAR * certName) = 0;

	virtual bool ImportPublicCert(const WCHAR * certName) = 0;

	virtual std::list<WCHAR *> GetCertList() const = 0;

	virtual BYTE * GetPublicKey(const WCHAR * certName) = 0;

};
#endif
