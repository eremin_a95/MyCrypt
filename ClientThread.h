//---------------------------------------------------------------------------

#ifndef ClientThreadH
#define ClientThreadH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.StdCtrls.hpp>
#include <System.SyncObjs.hpp>
#include <System.Win.ScktComp.hpp>
#include <Vcl.Dialogs.hpp>
#include "CryptoSystem.h"
//---------------------------------------------------------------------------
class ClientThread : public TThread
{
private:
	CryptoSystem * clientNetCrypt;
	HANDLE hClientDst;
	int clientFileSize;
	int clientCurrentFileSize;
	int clientBlockSize;
	WCHAR * clientDstFileName;
	TClientSocket * Client;
	TLabel * MainLabel;
    TOpenDialog * SendFileOpenDialog;

protected:
	void __fastcall Execute();
public:
    TEvent * clientRead, * clientSend;
	__fastcall ClientThread(CryptoSystem * clientNetCrypt, TClientSocket * Client, TLabel * MainLabel, TOpenDialog * SendFileOpenDialog, bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
