﻿//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop


#include "Main.h"
#include "CryptoProSystem.h"
#include <list>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#define SLEEP_DURATION 800

TMainForm *MainForm;
//---------------------------------------------------------------------------
__fastcall TMainForm::TMainForm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMainForm::OpenButtonClick(TObject *Sender)
{
	SourceFileOpenDialog->Execute();                    //Открыть диалог выбора файла
	if (SourceFileOpenDialog->FileName.data() != NULL)  //Если выбран файл
	{
		if (UsingKeyRadioGroup->ItemIndex == 0)
			StartButton->Enabled = true;
		else if ((UsingKeyRadioGroup->ItemIndex == 1) && (KeyFileOpenDialog->FileName.data() != NULL))
			StartButton->Enabled = true;
		else if ((UsingKeyRadioGroup->ItemIndex == 2) && (PublicKeyFileOpenDialog->FileName.data() != NULL))
			StartButton->Enabled = true;
		MainLabel->Font->Color = clWindowText;
		MainLabel->Font->Style = TFontStyles();
		MainLabel->Caption = L"Выберите режим работы, криптографический алгоритм и источник ключевой информации, после чего нажмите кнопку \"Выполнить преобразование\"";
	}
	else                                                //Если файл не выбран
	{
		StartButton->Enabled = false;
		MainLabel->Font->Color = clWindowText;
		MainLabel->Font->Style = TFontStyles();
        MainLabel->Caption = L"Выберите файл для зашифрования/расшифрования";
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::OpenKeyButtonClick(TObject *Sender)
{
	KeyFileOpenDialog->Execute();                       //Открыть диалог выбора файла ключа
    if (SourceFileOpenDialog->FileName.data() != NULL)
		if (KeyFileOpenDialog->FileName.data() != NULL)
			StartButton->Enabled = true;
}

//---------------------------------------------------------------------------


void __fastcall TMainForm::StartButtonClick(TObject *Sender)
{
	WCHAR * srcFile = SourceFileOpenDialog->FileName.c_str();
	WCHAR * keyFile = NULL;
	WCHAR * certFile = NULL;
	BYTE * password = NULL;

	bool conditionAcceptable = false;                       //Выполнены ли условия для выполнения преобразования
	if (UsingKeyRadioGroup->ItemIndex == 1)   				//Если выбран ключ из файла
	{
		if (KeyFileOpenDialog->FileName.data() != NULL)		//Если выбран ключевой файл
		{
			keyFile = KeyFileOpenDialog->FileName.c_str();  //Задаем имя ключевого файла
			conditionAcceptable = true;
		}
		else
		{                                                   //Если ключевой файл не выбран
        	MainLabel->Font->Color = clRed;
			MainLabel->Font->Style = TFontStyles() << fsBold;
			MainLabel->Caption = L"Не выбран ключевой файл!";	
        }
	}
	else if (UsingKeyRadioGroup->ItemIndex == 2)   				//Если выбран открытый ключ второй стороны
	{
		if (PublicKeyFileOpenDialog->FileName.data() != NULL)		//Если выбран файл сертиифката
		{
			certFile = PublicKeyFileOpenDialog->FileName.c_str();	//Задаем имя файла сертиифката
			conditionAcceptable = true;
		}
		else
		{                                                   //Если файл сертиифката не выбран
        	MainLabel->Font->Color = clRed;
			MainLabel->Font->Style = TFontStyles() << fsBold;
			MainLabel->Caption = L"Не выбран файл открытого ключа второй стороны!";
		}
    }
	else if ((UsingKeyRadioGroup->ItemIndex == 0)  && (PasswordEdit->Text != L""))  //Если выбран ключ из пароля и он не пустой
	{
		if ((ModeRadioGroup->ItemIndex == 0) && (PasswordEdit->Text == RepeatEdit->Text) ||	//Если выбрано "Шифрование" и пароль верно повторен
			(ModeRadioGroup->ItemIndex == 1))                								//Или выбрано "Расшифрование"
		{
			AnsiString ansi(PasswordEdit->Text);            //Читаем пароль в ANSI-строку
			password = new BYTE[ansi.Length() + 1];
			strcpy(password,ansi.c_str());					//Копируем пароль в буффер
			conditionAcceptable = true;
		}
		else
		{
        	MainLabel->Font->Color = clRed;
			MainLabel->Font->Style = TFontStyles() << fsBold;
			MainLabel->Caption = L"Введенные пароли не совпадают!"; 	
		}	
	}
	else
	{
		MainLabel->Font->Color = clRed;
		MainLabel->Font->Style = TFontStyles() << fsBold;
		MainLabel->Caption = L"Пароль не может быть пустым!";
    }

	if (conditionAcceptable)
	{
		int alg = SwitchAlg();                              //Номер криптоалгоритма
	

		fileCrypt = new CryptoProSystem();                         //Создаем объект криптосистемы
		bool openResult = false;
		if (keyFile != NULL)                                //Если указан ключевой файл
		{
			openResult = fileCrypt->Init(alg, keyFile);            //Инициализируем криптосистему по ключевому файлу
		}
		else if (certFile != NULL)                          //Если указан файл открытого ключа
		{
			openResult = fileCrypt->Init(alg, CryptoAlgorythms::EXCHANGE_DH, certFile);           //Инициализируем криптосистему по файлу открытого ключа
		}
		else if (password != NULL)                          //Если указан пароль
		{
			openResult = fileCrypt->Init(alg, password);           //Инициализируем криптосистему по паролю
            delete[] password;
		}
	

		if (openResult)           //Если криптосистема успешно проинициализирована
		{
			TargetFileSaveDialog->InitialDir = SourceFileOpenDialog->InitialDir;
			TargetFileSaveDialog->Execute();                     //Вызываем диалог выбора файла назначения
			WCHAR * dstFileName = NULL;
			if (TargetFileSaveDialog->FileName.data() != NULL)   //Если файл выбран
			{
				dstFileName = TargetFileSaveDialog->FileName.c_str();
				//TargetFileSaveDialog->FileName.Delete()     //очищаем строку???
			}
		
			bool cryptoResult = false;
			if (ModeRadioGroup->ItemIndex == 0)      //Если выбран режим зашифрования
			{
				cryptoResult = fileCrypt->EncryptFile(srcFile, dstFileName);
			}
			else                                     //Если выбран режим расшифрования
			{
				cryptoResult = fileCrypt->DecryptFile(srcFile, dstFileName);
			}
		
			if (cryptoResult)                        //Если успешно зашифровано/расшифровано
			{
				StartButton->Enabled = false;
				MainLabel->Font->Color = clGreen;
				MainLabel->Font->Style = TFontStyles() << fsBold;
				MainLabel->Caption = L"Преобразование успешно завершено!";
			}
			else                                     //Если не удалось зашифровать/расшифровать
			{
				MainLabel->Font->Color = clRed;
				MainLabel->Font->Style = TFontStyles() << fsBold;
				MainLabel->Caption = L"Ошибка при выполнении преобразования!";
			}
		}
		else                     //Если криптосистема не смогла проинициализироваться
		{
			MainLabel->Font->Color = clRed;
			MainLabel->Font->Style = TFontStyles() << fsBold;
			MainLabel->Caption = L"Ошибка инициализации криптографической системы!";
		}

		delete fileCrypt;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ModeRadioGroupClick(TObject *Sender)
{
	PasswordEdit->Text = L"";
	RepeatEdit->Text = L"";
    RepeatLabel->Font->Color = clWindowText;
	RepeatLabel->Font->Style = TFontStyles();

	if (ModeRadioGroup->ItemIndex == 0)                 //Режим "Шифрование"
	{
		if (UsingKeyRadioGroup->ItemIndex == 0)         //Если выбран ключ из пароля
		{
			RepeatEdit->Visible = true;
			RepeatLabel->Visible = true;
		}
	}
	else                                               //Режим "Расшифрование"
	{
		if (!ExportKeyCheckBox->Checked)
		{
			RepeatEdit->Visible = false;
			RepeatLabel->Visible = false;
		}
	}
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::UsingKeyRadioGroupClick(TObject *Sender)
{
	if (UsingKeyRadioGroup->ItemIndex == 0)				//По паролю
	{
		if (SourceFileOpenDialog->FileName.data() != NULL)
			StartButton->Enabled = true;

		OpenKeyButton->Visible = false;
		PasswordLabel->Visible = true;
		PasswordEdit->Visible = true;
		ExportKeyCheckBox->Visible = true;
		ExportKeyButton->Visible = true;
        OpenPublicKeyButton->Visible = false;

		PasswordEdit->Text = L"";
		RepeatEdit->Text = L"";

		if (ModeRadioGroup->ItemIndex == 0)				//Если выбран режим "Шифрование"
		{
            RepeatEdit->Visible = true;
			RepeatLabel->Visible = true;
			RepeatLabel->Font->Color = clWindowText;
			RepeatLabel->Font->Style = TFontStyles();
		}
	}
	else if (UsingKeyRadioGroup->ItemIndex == 1)		//По ключевому файлу
	{
		if (KeyFileOpenDialog->FileName.data() == NULL)
			StartButton->Enabled = false;

		ExportKeyCheckBox->Checked = false;

		OpenKeyButton->Visible = true;
		PasswordEdit->Visible = false;
		ExportKeyCheckBox->Visible = false;
		ExportKeyButton->Visible = false;
		PasswordLabel->Visible = false;
		OpenPublicKeyButton->Visible = false;

		RepeatEdit->Visible = false;
		RepeatLabel->Visible = false;
		RepeatLabel->Font->Color = clWindowText;
		RepeatLabel->Font->Style = TFontStyles();
	}
	else                                                //По сертификату получателя
	{
		ExportKeyCheckBox->Checked = false;

		OpenKeyButton->Visible = false;
		PasswordEdit->Visible = false;
		ExportKeyCheckBox->Visible = false;
		ExportKeyButton->Visible = false;
		PasswordLabel->Visible = false;
		OpenPublicKeyButton->Visible = true;

		RepeatEdit->Visible = false;
		RepeatLabel->Visible = false;
		RepeatLabel->Font->Color = clWindowText;
		RepeatLabel->Font->Style = TFontStyles();
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ExportKeyCheckBoxClick(TObject *Sender)
{
	if (ExportKeyCheckBox->Checked)
	{
		ExportKeyButton->Enabled = true;
		RepeatEdit->Visible = true;
		RepeatLabel->Visible = true;
	}
	else
	{
		if (ModeRadioGroup->ItemIndex == 1)				//Если выбран режим "Расшифрование"
		{
			RepeatEdit->Visible = false;
			RepeatLabel->Visible = false;
			RepeatEdit->Text = L"";
		}

        ExportKeyButton->Enabled = false;
    }
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ExportKeyButtonClick(TObject *Sender)
{
    if (PasswordEdit->Text != L"")                      //Если пароль не пустой
	{
		if (PasswordEdit->Text == RepeatEdit->Text)    //Если пароль совпадает с повторенным
		{
			KeyFileSaveDialog->Execute();                   //Открыть диалог выбора файла ключа

			if ((KeyFileSaveDialog->FileName.data() != NULL) )   //Если выбран файл
			{
					AnsiString ansi(PasswordEdit->Text);
					BYTE * password = ansi.c_str();

					int alg = SwitchAlg();

					fileCrypt = new CryptoProSystem();                   //Создаем объект криптосистемы
					fileCrypt->Init(alg, password);                      //Инициализируем криптосистему по паролю

					bool result = fileCrypt->ExportKey(KeyFileSaveDialog->FileName.c_str());
					if (result)
					{
						MainLabel->Font->Color = clGreen;
						MainLabel->Font->Style = TFontStyles() << fsBold;
						MainLabel->Caption = L"Файл ключа успешно экспортирован!";
					}
					else
					{
						MainLabel->Font->Color = clRed;
						MainLabel->Font->Style = TFontStyles() << fsBold;
						MainLabel->Caption = L"Не удалось экспортировать ключ в файл!";
					}

					delete fileCrypt;
			}
		}
		else
		{
            MainLabel->Font->Color = clRed;
			MainLabel->Font->Style = TFontStyles() << fsBold;
			MainLabel->Caption = L"Введенные пароли не совпадают!";
        }
	}
	else                                                //Если пароль пустой
	{
		MainLabel->Font->Color = clRed;
		MainLabel->Font->Style = TFontStyles() << fsBold;
		MainLabel->Caption = L"Пароль не может быть пустым!";
	}
}
//---------------------------------------------------------------------------



void __fastcall TMainForm::RepeatEditChange(TObject *Sender)
{
	if (RepeatEdit->Text == PasswordEdit->Text)
	{
		RepeatLabel->Font->Color = clGreen;
		RepeatLabel->Font->Style = TFontStyles() << fsBold;
	}
	else
	{
        RepeatLabel->Font->Color = clRed;
		RepeatLabel->Font->Style = TFontStyles() << fsBold;
    }
}
//---------------------------------------------------------------------------

int TMainForm::SwitchAlg()
{
	switch (AlgComboBox->ItemIndex)
	{
	case 0:
		return CryptoAlgorythms::MAGMA;
	default:
		return CryptoAlgorythms::NONE;
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::FormCreate(TObject *Sender)
{
	servNetCrypt = new CryptoProSystem();
	servNetCrypt->Init(CryptoAlgorythms::MAGMA,CryptoAlgorythms::EXCHANGE_DH,NULL);
	delete servNetCrypt;


	/*
	CryptoContainer * cc = new CryptoProContainer(L"CryptoProContainer");
	if (cc->IsOpened())
	{
		std::list<WCHAR *> certList = cc->GetCertList();
		for (std::list<WCHAR *>::iterator iterator = certList.begin(); iterator != certList.end(); ++iterator) {
			CertsComboBox->AddItem(*iterator, NULL);
		}
	}
	cc->Close();
    */
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::OpenPublicKeyButtonClick(TObject *Sender)
{
	PublicKeyFileOpenDialog->Execute();                       //Открыть диалог выбора файла сертификата
	if (SourceFileOpenDialog->FileName.data() != NULL)
		if (PublicKeyFileOpenDialog->FileName.data() != NULL)
			StartButton->Enabled = true;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::StartSessionButtonClick(TObject *Sender)
{
	Client->Active = false;
	Client->Address = IPEdit->Text;
	clientNetCrypt = new CryptoProSystem();
    clientThread = new ClientThread(clientNetCrypt, Client, MainLabel, SendFileOpenDialog, false);
	clientThread->clientRead->ResetEvent();
	clientThread->clientSend->ResetEvent();
	Client->Active = true;
	MainLabel->Font->Color = clWindowText;
	MainLabel->Font->Style = TFontStyles();
	MainLabel->Caption = L"Установка соединения ...";
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::SendFileButtonClick(TObject *Sender)	//Отправка файла
{
	SendFileOpenDialog->Execute();                    //Открыть диалог выбора файла
	if(SendFileOpenDialog->FileName.data() != NULL)  //Если выбран файл
	{
		clientThread->clientSend->SetEvent();
	}
	else
	{
        MainLabel->Font->Color = clWindowText;
		MainLabel->Font->Style = TFontStyles();
		MainLabel->Caption = L"Выберите файл для отправки";
	}
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ClientDisconnect(TObject *Sender, TCustomWinSocket *Socket)
//При разрыве соединения от сервера
{
	Client->Active = false;
    MainLabel->Font->Color = clRed;
	MainLabel->Font->Style = TFontStyles() << fsBold;
	MainLabel->Caption = L"Нет соединения с сервером!";
	clientThread->Suspend();
	SendFileButton->Enabled = false;
	delete clientNetCrypt;
	clientNetCrypt = NULL;
    DeleteFile(L"PublicKey2.pub");
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ClientError(TObject *Sender, TCustomWinSocket *Socket,
		  TErrorEvent ErrorEvent, int &ErrorCode)
// Если сервер не доступен при подключении
{
	ErrorCode=0;
    MainLabel->Font->Color = clRed;
	MainLabel->Font->Style = TFontStyles() << fsBold;
	MainLabel->Caption = L"Соединение с сервером потеряно!";
    clientThread->Suspend();
	delete clientNetCrypt;
	clientNetCrypt = NULL;
	SendFileButton->Enabled = false;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ClientConnect(TObject *Sender, TCustomWinSocket *Socket)
{
	SendFileButton->Enabled = true;
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::ClientRead(TObject *Sender, TCustomWinSocket *Socket)
{
    clientThread->clientRead->SetEvent();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::ServerClientDisconnect(TObject *Sender, TCustomWinSocket *Socket)
//если клиент отключился
{
	ServerListBox->AddItem(L"Клиент " + Socket->RemoteAddress + L":" + Socket->RemotePort + L" отсоединен.", NULL);
	DeleteFile(L"PublicKey2.pub");
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::ServerClientRead(TObject *Sender, TCustomWinSocket *Socket)
//полуение Открытых ключей и файлов
{
	serverThread->serverRead->SetEvent();
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::ServerListen(TObject *Sender, TCustomWinSocket *Socket)

{
	serverConnectThread = new ServerConnectThread(Server, false);
	serverConnectThread->serverClientConnected->ResetEvent();
    servNetCrypt = new CryptoProSystem();
	serverThread = new ServerThread(servNetCrypt, Server, ServerListBox, false);
	serverThread->serverRead->ResetEvent();
		//ServerListBox->AddItem(L"Сетевой адрес: " + Server->Socket->LocalHost, NULL);
	ServerListBox->AddItem(L"Сервер запущен на порту " + IntToStr(Server->Port) + L".", NULL);
}
//---------------------------------------------------------------------------


void __fastcall TMainForm::SwitchTabControlChange(TObject *Sender)
{
	if (SwitchTabControl->TabIndex == 0)    	//Если выбрана вкладка "Шифрование файлов"
	{
		CryptoModeGroupBox->Visible = true;
		ParamsGroupBox->Visible = true;
		NetGroupBox->Visible = false;
	}
	else if (SwitchTabControl->TabIndex == 1)    	//Если выбрана вкладка "Отправка файлов"
	{
		CryptoModeGroupBox->Visible = false;
		ParamsGroupBox->Visible = false;
		NetGroupBox->Visible = true;
	}

}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ServerClientError(TObject *Sender, TCustomWinSocket *Socket,
          TErrorEvent ErrorEvent, int &ErrorCode)
{
    ServerListBox->AddItem(L"Соединение с клиентом " + Socket->RemoteAddress + L":" + Socket->RemotePort + L" потеряно.", NULL);
	DeleteFile(L"PublicKey2.pub");
    delete serverThread;
}
//---------------------------------------------------------------------------

void __fastcall TMainForm::ServerClientConnect(TObject *Sender, TCustomWinSocket *Socket)

{
    serverConnectThread->serverClientConnected->SetEvent();
    ServerListBox->AddItem(L"Присоединен клиент " + Socket->RemoteAddress + L":" + Socket->RemotePort + L".", NULL);
}
//---------------------------------------------------------------------------

