﻿//---------------------------------------------------------------------------

#ifndef CryptoSystemH
#define CryptoSystemH

#include <Windows.h>
#include <wincrypt.h>
//---------------------------------------------------------------------------

enum CryptoAlgorythms
{
    NONE = 0,
	MAGMA,
	KUZNECHIK,
	DES,
	TRIPLE_DES,
	AES,
	BLOWFISH,
    EXCHANGE_DH
};



class CryptoSystem
{
private:

protected:
	int algCsp;						//Код криптографического алгоритма в криптопровайдере
	HCRYPTPROV hProv;   		    //Дескриптор криптопровайдера
	HCRYPTKEY hSessionKey;			//Криптографический ключ

public:
	virtual ~CryptoSystem();

	/*
	 * Инициализирует криптосистему:
	 *   alg - код криптографического алгоритма;
	 *   password - пароль для вычисления ключа;
	 *   keyFile - имя файла, содержащего ключ.
	 * Возвращает true, если инициализация прошла успешно, false, если неудачно (файл не открылся, не заданы )
	 */
	virtual bool Init(int alg, const BYTE * password) = 0;
	virtual bool Init(int alg, const WCHAR * keyFile) = 0;
    virtual bool Init(int symAlg, int keyExchangeAlg, const WCHAR * peerCertFileName) = 0;

    /*
	 * Шифрует блок данных:
	 *   buffer - шифруемый блок, зашифрованные данные записываются в него же;
	 *   final - флаг последнего блока;
	 *   nEnc - указатель на ячейку памяти, в которую будет записано количество зашифрованных байт;
     *   blockSize - размер шифруемого блока.
	 * Возвращает true, если шифрование прошло успешно, false, если неудачно.
	 */
	virtual bool Encrypt(BYTE * buffer, bool final, DWORD * nEnc, int blockSize) = 0;

	/*
	 * Расшифровывает блок данных:
	 *   buffer - зашифрованные данные, расшифрованные данные записываются в него же;
	 *   final - флаг последнего блока;
	 *   nDec - указатель на ячейку памяти, в которую будет записано количество расшифрованных байт.
	 * Возвращает true, если расшифрование прошло успешно, false, если неудачно.
	 */
	virtual bool Decrypt(BYTE * buffer, bool final, DWORD * nDec) = 0;

	/*
	 * Шифрует файл:
	 *   fileName - имя шифруемого файла;
	 *   dstFileName - имя файла, в который будут записаны зашифрованные данные.
	 * Возвращает true, если шифрование прошло успешно, false, если неудачно.
	 */
	virtual bool EncryptFile(const WCHAR * fileName, const WCHAR * dstFileName) = 0;

	/*
	 * Расшифровывает файл:
	 *   fileName - имя расшифровываемого файла;
	 *   dstFileName - имя файла, в который будут записаны расшифрованные данные.
	 * Возвращает true, если расшифрование прошло успешно, false, если неудачно.
	 */
	virtual bool DecryptFile(const WCHAR * fileName, const WCHAR * dstFileName) = 0;



	virtual bool ExportKey(const WCHAR * keyFileName) = 0;
    virtual bool ExportKey(BYTE ** sessionKeyBlob, DWORD * sessionKeyBlobLength) = 0;
};





#endif
