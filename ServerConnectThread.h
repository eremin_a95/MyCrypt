//---------------------------------------------------------------------------

#ifndef ServerConnectThreadH
#define ServerConnectThreadH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.Win.ScktComp.hpp>
#include <System.SyncObjs.hpp>
//---------------------------------------------------------------------------
class ServerConnectThread : public TThread
{
private:
    TServerSocket * Server;
protected:
	void __fastcall Execute();
public:
    TEvent * serverClientConnected;
	__fastcall ServerConnectThread(TServerSocket * Server, bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
