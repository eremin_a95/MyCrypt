﻿//---------------------------------------------------------------------------

#pragma hdrstop

#include "CryptoProSystem.h"
#include "WinCryptEx.h"
#include <fstream>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#define BLOCK_SIZE 4096
#define MAX_PUBLIC_KEY_BLOB_SIZE 200
CryptoProSystem::CryptoProSystem()
{
	hProv = 0;
	hSessionKey = 0;
    hAgreeKey = 0;
	algCsp = CryptoAlgorythms::NONE;
    password = NULL;
}

CryptoProSystem::~CryptoProSystem()
{
	if (hAgreeKey)
		CryptDestroyKey(hAgreeKey);

	if (password != NULL)
	{
        delete[] password;
	}
}

bool CryptoProSystem::Init(int alg, const BYTE * password)
{
	algCsp = SwitchAlg(alg);						//Переводим код алгоритма из CryptoAlgorythms в код алгоритма в CSP

	bool result = true;
	result &= CryptAcquireContext(      		  	//Создаем контейнер
		&hProv,
		NULL,
		NULL,
		PROV_GOST_2001_DH,
		CRYPT_VERIFYCONTEXT);

	HCRYPTHASH hHash = 0;

	result &= CryptCreateHash(         			  //Создаем объект хэша
		hProv,
		CALG_GR3411,
		0,
		0,
		&hHash);

	int length = strlen(password);					//Длина пароля
	this->password = new BYTE[length+1];
	memcpy(this->password, password, length);		//Копируем пароль в приватный массив
    this->password[length] = '\0';
	result &= CryptHashData(						//Хэшируем пароль
		hHash,
		password,
		length,
		0);

	result &= CryptDeriveKey(						//Создаем ключ на основе хэша пароля
				hProv,
                algCsp,
				hHash,
				CRYPT_EXPORTABLE,
				&hSessionKey);

	if(hHash)
		CryptDestroyHash(hHash);					//Уничтожаем объект хэша

	return result;
}


bool CryptoProSystem::Init(int alg, const WCHAR * keyFile)
{
	algCsp = SwitchAlg(alg);						//Переводим код алгоритма из CryptoAlgorythms в код алгоритма в CSP

	bool result = true;
	result &= CryptAcquireContext(					//Создаем контейнер
		&hProv,
		NULL,
		NULL,
		PROV_GOST_2001_DH,
		CRYPT_VERIFYCONTEXT);

	HCRYPTHASH hHash = 0;

	result &= CryptCreateHash(						//Создаем объект хэша
		hProv,
		CALG_GR3411,
		0,
		0,
		&hHash);

	DWORD hashLength = 0;
	result &= CryptGetHashParam(					//Узнаем длину хэша
					  hHash,
					  HP_HASHVAL,
					  NULL,
					  &hashLength,
					  0);

	HANDLE hSrc = CreateFileW(						//Открываем файл источника
					keyFile,
					GENERIC_READ,
					FILE_SHARE_READ | FILE_SHARE_WRITE,
					NULL,
					OPEN_EXISTING,
					FILE_ATTRIBUTE_NORMAL,
					NULL);
	if (hSrc == INVALID_HANDLE_VALUE)
		return false;

	LARGE_INTEGER offset;							//Смещение в файле источника
	offset.QuadPart = 0;

	BYTE * hash = new BYTE[hashLength];             //Значение хэша
	DWORD nRead = 0;
	result &= ReadFile(								//Читаем хэш из файла
				hSrc,
				hash,
				hashLength,
				&nRead,
				NULL);
	result &= (nRead == hashLength);

	result &= CryptSetHashParam(					//Устанавливаем хэш
					  hHash,
					  HP_HASHVAL,
					  hash,
					  0);

	result &= CryptDeriveKey(						//Создаем ключ на основе хэша пароля
				hProv,
				algCsp,
				hHash,
				CRYPT_EXPORTABLE,
				&hSessionKey);

	if(hHash)
		CryptDestroyHash(hHash);     //Уничтожаем объект хэша

	delete[] hash;
	return result;
}


bool CryptoProSystem::Init(int symAlg, int keyExchangeAlg, const WCHAR * peerPublicKeyFileName)
{
	if (keyExchangeAlg != CryptoAlgorythms::EXCHANGE_DH)
		return false;

    algCsp = SwitchAlg(symAlg);

	bool result = false;
	result = CryptAcquireContext(					//Пытаемся открыть контейнер
						&hProv,
						L"MyCryptoContainer",
						NULL,
						PROV_GOST_2012_256,
						0);

    if (!result)
	{
		result = CryptAcquireContext(					//Создаем контейнер
						&hProv,
						L"MyCryptoContainer",
						NULL,
						PROV_GOST_2012_256,
						CRYPT_NEWKEYSET);
	}

	if (!result)
	{
        return false;
	}


	HCRYPTKEY hPrivKey = 0;								//Закрытый ключ пользователя
    result &= CryptGetUserKey(							//Получение закрытого ключа пользователя
				hProv,
				AT_KEYEXCHANGE,
				&hPrivKey);
	if (!result)                                      //Если не удалось получить закрытый ключ
	{
        result &= CryptGenKey(                          //Генерируем закрытый ключ для обмена
					hProv,
					AT_KEYEXCHANGE,
					0,
					&hPrivKey);

		result &= ExportPublicKey(hPrivKey, L"PublicKey.pub");    //При генерации записываем открытый ключ в файл
	}


	if (!result)
		return false;

	if (peerPublicKeyFileName == NULL)
	{
        return false;
	}

	HANDLE hPubKeyFile = CreateFileW(						//Открываем файл открытого ключа
							peerPublicKeyFileName,
							GENERIC_READ,
							FILE_SHARE_READ | FILE_SHARE_WRITE,
							NULL,
							OPEN_EXISTING,
							FILE_ATTRIBUTE_NORMAL,
							NULL);

	DWORD pubKeyBlobLength = MAX_PUBLIC_KEY_BLOB_SIZE;
	BYTE * pubKeyBlob = new BYTE[pubKeyBlobLength];
    //DWORD nRead = 0;
    result &= ReadFile(								//Читаем сертификат из файла
				hPubKeyFile,
				pubKeyBlob,
				pubKeyBlobLength,
				&pubKeyBlobLength,
				NULL);

	//if (!result)
	//	return false;

	result &= CryptImportKey(                               //Получение ключа согласования на
				hProv,
				pubKeyBlob,                                 //открытом ключе получатея и
				pubKeyBlobLength,
				hPrivKey,                                   //закрытом ключе отправителя
				0,
				&hAgreeKey);



	ALG_ID agreeKeyAlg = CALG_PRO12_EXPORT;
	result &= CryptSetKeyParam(                             //Установление PRO12_EXPORT алгоритма ключа согласования
				hAgreeKey,
				KP_ALGID,
				(LPBYTE)&agreeKeyAlg,
				0);

	result &= CryptGenKey(                                  //Генерируем сессионный ключ
				hProv,
				algCsp,
				CRYPT_EXPORTABLE,
				&hSessionKey);

	delete[] pubKeyBlob;
	CloseHandle(hPubKeyFile);
    return result;
}


bool CryptoProSystem::Encrypt(BYTE * buffer, bool final, DWORD * nEnc, int blockSize)
{
	return CryptEncrypt(
				hSessionKey,
				0,
				final,
				0,
				buffer,
				nEnc,
				blockSize);
}


bool CryptoProSystem::Decrypt(BYTE * buffer, bool final, DWORD * nDec)
{
	return CryptDecrypt(
				hSessionKey,
				0,
				final,
				0,
				buffer,
				nDec);
}


bool CryptoProSystem::EncryptFile(const WCHAR * fileName, const WCHAR * dstFileName)
{
	HANDLE hSrc = CreateFileW(                  //Открываем файл источника
					fileName,
					GENERIC_READ,
					FILE_SHARE_READ | FILE_SHARE_WRITE,
					NULL,
					OPEN_EXISTING,
					FILE_ATTRIBUTE_NORMAL,
					NULL);
	if (hSrc == INVALID_HANDLE_VALUE)
		return false;

	HANDLE hDst = CreateFileW(                 //Открываем файл назначения
					dstFileName,
					GENERIC_WRITE,
					0,
					NULL,
					CREATE_ALWAYS,
					FILE_ATTRIBUTE_NORMAL,
					NULL);
    if (hDst == INVALID_HANDLE_VALUE)
		return false;

	bool result = true;                       	//Успешно ли завершается функция
	DWORD nWrote = 0;

	//Если усть hAgreeKey, то записываем зашифрованный сессионный ключ
	if (hAgreeKey != 0)
	{
		BYTE * encSesKeyBlob = NULL;
		DWORD encSesKeyBlobLength;

		result &= ExportKey(&encSesKeyBlob, &encSesKeyBlobLength);

		DWORD encParamLength = (DWORD)((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bEncryptionParamSet[1]
				+ sizeof((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bEncryptionParamSet[0]
				+ sizeof((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bEncryptionParamSet[1]; //Так написано в примере ¯\_(ツ)_/¯

		result &= WriteFile(                       	//В файл назначения записываем длину параметров
					hDst,
					&encParamLength,
					sizeof(encParamLength),
					&nWrote,
					NULL);

		result &= WriteFile(                       	//В файл назначения записываем параметры
					hDst,
					((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bEncryptionParamSet,
					encParamLength,
					&nWrote,
					NULL);

		result &= WriteFile(                       	//В файл назначения записываем IV ключа согласования
					hDst,
					((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bSV,
					SEANCE_VECTOR_LEN,
					&nWrote,
					NULL);

		result &= WriteFile(                       	//В файл назначения записываем зашифрованный сессионный ключ
					hDst,
					((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bEncryptedKey,
					G28147_KEYLEN,
					&nWrote,
					NULL);

		result &= WriteFile(                       	//В файл назначения записываем MacKey
					hDst,
					((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bMacKey,
					EXPORT_IMIT_SIZE,
					&nWrote,
					NULL);

        //CRYPT_SIMPLEBLOB * p = (CRYPT_SIMPLEBLOB*)encSesKeyBlob;

		if (encSesKeyBlob != NULL)
			delete[] encSesKeyBlob;
	}


	DWORD sIV = 0;                            	//Размер IV
	BYTE * iv;                         			//IV
	result &= CryptGetKeyParam(               	//Узнаем размер IV
				hSessionKey,
				KP_IV,
				NULL,
				&sIV,
				0);
	iv = new BYTE[sIV];
	result &= CryptGetKeyParam(               	//Узнаем IV
				hSessionKey,
				KP_IV,
				iv,
				&sIV,
				0);

	result &= WriteFile(                       	//В файл назначения записываем IV
				hDst,
				iv,
				sIV,
				&nWrote,
				NULL);
	result &= (nWrote == sIV);


	LARGE_INTEGER offset;                     	//Смещение в файле источника
	offset.QuadPart = 0;
	bool eof = false;                         	//Закончился ли файл
	BYTE sBuffer[BLOCK_SIZE];
	while (!eof && result)                    	//Пока не закончился файл или не произошла ощибка
	{
		DWORD newPos = SetFilePointer(       	//Перемещаемся в нужное место в файле
						hSrc,
						offset.LowPart,
						&offset.HighPart,
						FILE_BEGIN);
		result &= (newPos == offset.QuadPart);

		DWORD nRead = 0;
		result &= ReadFile(                 //Читаем блок данных
					hSrc,
					sBuffer,
					BLOCK_SIZE,
					&nRead,
					NULL);

		if (!result)
		{
			return false;
		}

		eof = (nRead < BLOCK_SIZE);

		result &= Encrypt(sBuffer, eof, &nRead, BLOCK_SIZE); //Зашифровываем

		result &= WriteFile(                 //Записываем зашифрованные данные в файл
					hDst,
					sBuffer,
					nRead,
					&nWrote,
					NULL);
		offset.QuadPart += nRead;           //Сдвигаемся дальше в файле
	}
	CloseHandle(hSrc);
	CloseHandle(hDst);
	delete[] iv;
	return result;
}


bool CryptoProSystem::DecryptFile(const WCHAR * fileName, const WCHAR * dstFileName)
{
	HANDLE hSrc = CreateFileW(                  //Открываем файл источника
					fileName,
					GENERIC_READ,
					FILE_SHARE_READ | FILE_SHARE_WRITE,
					NULL,
					OPEN_EXISTING,
					FILE_ATTRIBUTE_NORMAL,
					NULL);
	if (hSrc == INVALID_HANDLE_VALUE)
		return false;

	HANDLE hDst = CreateFileW(                 //Открываем файл назначения
					dstFileName,
					GENERIC_WRITE,
					0,
					NULL,
					CREATE_ALWAYS,
					FILE_ATTRIBUTE_NORMAL,
					NULL);
	if (hDst == INVALID_HANDLE_VALUE)
		return false;


	bool result = true;                       	//Успешно ли завершается функция
	LARGE_INTEGER offset;                     	//Смещение в файле источника
	offset.QuadPart = 0;
	DWORD nRead = 0;

	if (hAgreeKey != 0)                         //Если задан ключ согласования, считываем зашифрованный сессионный ключ
	{
		//TODO: реализовать
		DWORD encParamLength;
        result &= ReadFile(                      	//Читаем длину параметров
					hSrc,
					&encParamLength,
					sizeof(encParamLength),
					&nRead,
					NULL);

		BYTE * encParam = new BYTE[encParamLength];
		result &= ReadFile(                      	//Читаем параметроы
					hSrc,
					encParam,
					encParamLength,
					&nRead,
					NULL);

		DWORD encSesKeyBlobLength = encParamLength + sizeof(CRYPT_SIMPLEBLOB_HEADER) + SEANCE_VECTOR_LEN +
			G28147_KEYLEN + EXPORT_IMIT_SIZE;// +sizeof(pbEncryptionParamSetStandart);
		BYTE * encSesKeyBlob = new BYTE[encSesKeyBlobLength];

		//Поля BLOB'а
		CRYPT_SIMPLEBLOB_HEADER defaultBlobHeader;
		defaultBlobHeader.BlobHeader.aiKeyAlg = CALG_G28147;
		defaultBlobHeader.BlobHeader.bType = SIMPLEBLOB;
		defaultBlobHeader.BlobHeader.bVersion = BLOB_VERSION;
		defaultBlobHeader.BlobHeader.reserved = 0;
		defaultBlobHeader.EncryptKeyAlgId = CALG_G28147;
		defaultBlobHeader.Magic = G28147_MAGIC;

		//Копируем постоянные поля BLOB'а в созданный BLOB
		memcpy(&((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->tSimpleBlobHeader, &defaultBlobHeader, sizeof(CRYPT_SIMPLEBLOB_HEADER));

		result &= ReadFile(                      	//Читаем IV ключа согласования из файла
					hSrc,
					((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bSV,
					SEANCE_VECTOR_LEN,
					&nRead,
					NULL);

		result &= ReadFile(                      	//Читаем зашифрованный сессионный ключ
					hSrc,
					((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bEncryptedKey,
					G28147_KEYLEN,
					&nRead,
					NULL);

		memcpy(((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bEncryptionParamSet, encParam, encParamLength);

        result &= ReadFile(                      	//Читаем MacKey
					hSrc,
					((CRYPT_SIMPLEBLOB*)encSesKeyBlob)->bMacKey,
					EXPORT_IMIT_SIZE,
					&nRead,
					NULL);

		result &= CryptImportKey(
					hProv,
					encSesKeyBlob,
					encSesKeyBlobLength,
					hAgreeKey,
					0,
					&hSessionKey);

		//CRYPT_SIMPLEBLOB * p = (CRYPT_SIMPLEBLOB*)encSesKeyBlob;
		delete[] encSesKeyBlob;
		if (!result)
            return false;
	}

	DWORD sIV = 0;                            	//Размер IV
	BYTE * iv;                         			//IV
	result &= CryptGetKeyParam(               	//Узнаем размер IV
				hSessionKey,
				KP_IV,
				NULL,
				&sIV,
				0);
	iv = new BYTE[sIV];


	result &= ReadFile(                      	//Читаем IV из файла
				hSrc,
				iv,
				sIV,
				&nRead,
				NULL);
	result &= (nRead == sIV);

	result &= CryptSetKeyParam(             	//Устанавливаем IV
				   hSessionKey,
				   KP_IV,
				   iv,
				   0);

	DWORD nWrote = 0;
	//offset.QuadPart = sIV;
	bool eof = false;                         	//Закончился ли файл
    BYTE sBuffer[BLOCK_SIZE];
	while (!eof && result)                    	//Пока не закончился файл или не произошла ощибка
	{
		/*DWORD newPos = SetFilePointer(        	//Перемещаемся в нужное место в файле
							hSrc,
							offset.LowPart,
							&offset.HighPart,
							FILE_BEGIN);
		result &= (newPos == offset.QuadPart);*/

		result &= ReadFile(                  	//Читаем блок данных
					hSrc,
					sBuffer,
					BLOCK_SIZE,
					&nRead,
					NULL);

		if (!result)
		{
			return false;
		}

		eof = (nRead < BLOCK_SIZE);

		result &= Decrypt(sBuffer, eof, &nRead);    //Расшифровываем

		result &= WriteFile(                        //Записываем расшифрованные данные в файл
					hDst,
					sBuffer,
					nRead,
					&nWrote,
					NULL);
        offset.QuadPart += nRead;                //Сдвигаемся дальше в файле
	}
	CloseHandle(hSrc);
	CloseHandle(hDst);
	delete[] iv;
	return result;
}


bool CryptoProSystem::ExportKey(const WCHAR * keyFileName)
{
	bool result = false;
	if (hSessionKey)
	{
        HCRYPTHASH hHash = 0;
        result = true;
		result &= CryptCreateHash(         			  //Создаем объект хэша
					hProv,
					CALG_GR3411,
					0,
					0,
					&hHash);

		int length = strlen(password);					//Длина пароля

		result &= CryptHashData(        			   //Хэшируем пароль
					hHash,
					password,
					length,
					0);

		 BYTE * hash;
         DWORD hashLength;
		 result &= CryptGetHashParam(    			  //Узнаем длину хэша
					  hHash,
					  HP_HASHVAL,
					  NULL,
					  &hashLength,
					  0);

		hash = new BYTE[hashLength];
		result &= CryptGetHashParam(      			 //Читаем значение хэша
					  hHash,
					  HP_HASHVAL,
					  hash,
					  &hashLength,
					  0);

		HANDLE hDst = CreateFileW(                 //Открываем файл назначения
						keyFileName,
						GENERIC_WRITE,
						0,
						NULL,
						CREATE_ALWAYS,
						FILE_ATTRIBUTE_NORMAL,
						NULL);
		if (hDst == INVALID_HANDLE_VALUE)
			return false;

		DWORD nWrote = 0;
        result &= WriteFile(
					hDst,
					hash,
					hashLength,
					&nWrote,
					NULL);
		result &= (nWrote == hashLength);

        if(hHash)
			CryptDestroyHash(hHash);    			 //Уничтожаем объект хэша
		CloseHandle(hDst);
		//delete[] key;
		delete[] hash;
	}
	return result;
}


bool CryptoProSystem::ExportKey(BYTE ** sessionKeyBlob, DWORD * sessionKeyBlobLength)
{
	bool result = CryptExportKey(                   //Узнаем длину BLOB'а зашифрвоанного сессионного ключа
					hSessionKey,
					hAgreeKey,
					SIMPLEBLOB,
					0,
					NULL,
					sessionKeyBlobLength);

	if (!result)
			return false;

	*sessionKeyBlob = new BYTE[*sessionKeyBlobLength];
	result &= CryptExportKey(                   //Экспортируем зашифрованный сессионный ключ в BLOB
					hSessionKey,
					hAgreeKey,
					SIMPLEBLOB,
					0,
					*sessionKeyBlob,
					sessionKeyBlobLength);

	return result;
}


ALG_ID CryptoProSystem::SwitchAlg(int externalAlg)
{
	switch (externalAlg)
	{
	case CryptoAlgorythms::MAGMA:
		return CALG_G28147;
    default:
        return 0;
	}

}


bool CryptoProSystem::ExportPublicKey(HCRYPTKEY hKey, const WCHAR * fileName)
{
	HANDLE hPubKeyFile = CreateFileW(                 //Создаем файл открытого ключа
							fileName,
							GENERIC_WRITE,
							0,
							NULL,
							CREATE_ALWAYS,
							FILE_ATTRIBUTE_NORMAL,
							NULL);

    DWORD pubKeyBlobSize = MAX_PUBLIC_KEY_BLOB_SIZE;
	BYTE * pubKeyBlob = new BYTE[pubKeyBlobSize];
	bool result = CryptExportKey(                         //Экспортируем открытый ключ
					hKey,
					NULL,
					PUBLICKEYBLOB,
					0,
					pubKeyBlob,
					&pubKeyBlobSize);


    DWORD nWrote = 0;
	result &= WriteFile(							//Записываем открытый ключ в файл
					hPubKeyFile,
					pubKeyBlob,
					pubKeyBlobSize,
					&nWrote,
					NULL);

    CloseHandle(hPubKeyFile);

    return result;
}
