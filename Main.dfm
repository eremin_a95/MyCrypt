object MainForm: TMainForm
  Left = 100
  Top = 100
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'MyCrypt'
  ClientHeight = 381
  ClientWidth = 481
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  Scaled = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object MainLabel: TLabel
    Left = 8
    Top = 320
    Width = 465
    Height = 50
    AutoSize = False
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1076#1083#1103' '#1079#1072#1096#1080#1092#1088#1086#1074#1072#1085#1080#1103'/'#1088#1072#1089#1096#1080#1092#1088#1086#1074#1072#1085#1080#1103
    WordWrap = True
  end
  object SwitchTabControl: TTabControl
    Left = 8
    Top = 8
    Width = 465
    Height = 306
    TabOrder = 0
    Tabs.Strings = (
      #1064#1080#1092#1088#1086#1074#1072#1085#1080#1077' '#1092#1072#1081#1083#1086#1074
      #1054#1090#1087#1088#1072#1074#1082#1072' '#1092#1072#1081#1083#1086#1074)
    TabIndex = 0
    OnChange = SwitchTabControlChange
    object CryptoModeGroupBox: TGroupBox
      Left = 13
      Top = 32
      Width = 197
      Height = 265
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
      TabOrder = 0
      object OpenButton: TButton
        Left = 8
        Top = 15
        Width = 172
        Height = 25
        Caption = #1042#1099#1073#1088#1072#1090#1100' '#1092#1072#1081#1083
        TabOrder = 0
        OnClick = OpenButtonClick
      end
      object ModeRadioGroup: TRadioGroup
        Left = 3
        Top = 42
        Width = 180
        Height = 143
        Caption = #1056#1077#1078#1080#1084
        ItemIndex = 0
        Items.Strings = (
          #1047#1072#1096#1080#1092#1088#1086#1074#1072#1090#1100
          #1056#1072#1089#1096#1080#1092#1088#1086#1074#1072#1090#1100)
        TabOrder = 1
        OnClick = ModeRadioGroupClick
      end
      object AlgComboBox: TComboBox
        Left = 3
        Top = 191
        Width = 179
        Height = 21
        ItemIndex = 0
        TabOrder = 2
        Text = #1043#1054#1057#1058' 28147-89 ('#1052#1072#1075#1084#1072')'
        Items.Strings = (
          #1043#1054#1057#1058' 28147-89 ('#1052#1072#1075#1084#1072')')
      end
      object StartButton: TButton
        Left = 3
        Top = 224
        Width = 180
        Height = 25
        Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1077
        Enabled = False
        TabOrder = 3
        OnClick = StartButtonClick
      end
    end
    object ParamsGroupBox: TGroupBox
      Left = 216
      Top = 32
      Width = 231
      Height = 265
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1082#1083#1102#1095#1077#1074#1086#1081' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080
      TabOrder = 1
      object PasswordLabel: TLabel
        Left = 32
        Top = 97
        Width = 169
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = #1042#1074#1077#1076#1080#1090#1077' '#1087#1072#1088#1086#1083#1100
      end
      object RepeatLabel: TLabel
        Left = 32
        Top = 147
        Width = 169
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = #1055#1086#1074#1090#1086#1088#1080#1090#1077' '#1087#1072#1088#1086#1083#1100
      end
      object PasswordEdit: TEdit
        Left = 32
        Top = 120
        Width = 169
        Height = 21
        PasswordChar = '*'
        TabOrder = 0
      end
      object ExportKeyCheckBox: TCheckBox
        Left = 19
        Top = 193
        Width = 209
        Height = 17
        Caption = #1069#1082#1089#1087#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1082#1083#1102#1095' '#1074' '#1092#1072#1081#1083
        TabOrder = 1
        OnClick = ExportKeyCheckBoxClick
      end
      object OpenKeyButton: TButton
        Left = 19
        Top = 116
        Width = 180
        Height = 25
        Caption = #1042#1099#1073#1088#1072#1090#1100' '#1092#1072#1081#1083' '#1082#1083#1102#1095#1072
        TabOrder = 2
        Visible = False
        OnClick = OpenKeyButtonClick
      end
      object ExportKeyButton: TButton
        Left = 19
        Top = 224
        Width = 180
        Height = 25
        Caption = #1042#1099#1073#1088#1072#1090#1100' '#1092#1072#1081#1083' '#1082#1083#1102#1095#1072
        Enabled = False
        TabOrder = 3
        OnClick = ExportKeyButtonClick
      end
      object UsingKeyRadioGroup: TRadioGroup
        Left = 3
        Top = 12
        Width = 215
        Height = 79
        Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100
        ItemIndex = 0
        Items.Strings = (
          #1055#1072#1088#1086#1083#1100
          #1060#1072#1081#1083' '#1082#1083#1102#1095#1072
          #1054#1090#1082#1088#1099#1090#1099#1081' '#1082#1083#1102#1095' '#1074#1090#1086#1088#1086#1081' '#1089#1090#1086#1088#1086#1085#1099)
        TabOrder = 4
        OnClick = UsingKeyRadioGroupClick
      end
      object RepeatEdit: TEdit
        Left = 32
        Top = 166
        Width = 169
        Height = 21
        PasswordChar = '*'
        TabOrder = 5
        OnChange = RepeatEditChange
      end
      object OpenPublicKeyButton: TButton
        Left = 17
        Top = 97
        Width = 182
        Height = 44
        Caption = #1042#1099#1073#1088#1072#1090#1100' '#1092#1072#1081#1083' '#1086#1090#1082#1088#1099#1090#1086#1075#1086' '#1082#1083#1102#1095#1072' '#1074#1090#1086#1088#1086#1081' '#1089#1090#1086#1088#1086#1085#1099
        TabOrder = 6
        Visible = False
        WordWrap = True
        OnClick = OpenPublicKeyButtonClick
      end
    end
    object NetGroupBox: TGroupBox
      Left = 13
      Top = 32
      Width = 436
      Height = 257
      Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1077#1088#1077#1076#1072#1095#1080
      TabOrder = 2
      Visible = False
      object IPLabel: TLabel
        Left = 3
        Top = 16
        Width = 145
        Height = 13
        Caption = #1042#1074#1077#1076#1080#1090#1077' ip-'#1072#1076#1088#1077#1089' '#1087#1086#1083#1091#1072#1090#1077#1083#1103
      end
      object StartSessionButton: TButton
        Left = 3
        Top = 62
        Width = 174
        Height = 25
        Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1089#1086#1077#1076#1080#1085#1077#1085#1080#1077
        TabOrder = 0
        OnClick = StartSessionButtonClick
      end
      object SendFileButton: TButton
        Left = 3
        Top = 93
        Width = 174
        Height = 25
        Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1100' '#1092#1072#1081#1083
        Enabled = False
        TabOrder = 1
        OnClick = SendFileButtonClick
      end
      object IPEdit: TEdit
        Left = 3
        Top = 35
        Width = 175
        Height = 21
        TabOrder = 2
        Text = '127.0.0.1'
      end
      object ServerListBox: TListBox
        Left = 3
        Top = 124
        Width = 422
        Height = 114
        BorderStyle = bsNone
        ItemHeight = 13
        TabOrder = 3
      end
    end
  end
  object SourceFileOpenDialog: TOpenDialog
    Title = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1076#1083#1103' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1103
    Left = 360
    Top = 200
  end
  object KeyFileOpenDialog: TOpenDialog
    Title = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1082#1083#1102#1095#1072
    Left = 176
    Top = 200
  end
  object TargetFileSaveDialog: TSaveDialog
    Left = 56
    Top = 246
  end
  object KeyFileSaveDialog: TSaveDialog
    Left = 56
    Top = 198
  end
  object PublicKeyFileOpenDialog: TOpenDialog
    Title = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1089#1077#1088#1090#1080#1092#1080#1082#1072#1090#1072' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1103
    Left = 176
    Top = 248
  end
  object SendFileOpenDialog: TOpenDialog
    Left = 296
    Top = 248
  end
  object Client: TClientSocket
    Active = False
    ClientType = ctNonBlocking
    Port = 12345
    OnConnect = ClientConnect
    OnDisconnect = ClientDisconnect
    OnRead = ClientRead
    OnError = ClientError
    Left = 408
    Top = 248
  end
  object Server: TServerSocket
    Active = True
    Port = 12345
    ServerType = stNonBlocking
    OnListen = ServerListen
    OnClientConnect = ServerClientConnect
    OnClientDisconnect = ServerClientDisconnect
    OnClientRead = ServerClientRead
    OnClientError = ServerClientError
    Left = 360
    Top = 248
  end
end
