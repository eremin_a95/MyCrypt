//---------------------------------------------------------------------------

#include <System.hpp>
#pragma hdrstop

#include "ServerThread.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(&UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall ServerThread::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------
#define WAITING_DELAY 10000

__fastcall ServerThread::ServerThread(CryptoSystem * servNetCrypt, TServerSocket * Server, TListBox * ServerListBox, bool CreateSuspended)
	: TThread(CreateSuspended)
{
	FreeOnTerminate = true;
	this->servNetCrypt = servNetCrypt;
	this->Server = Server;
	this->ServerListBox = ServerListBox;
	serverRead = new TEvent(NULL, true, false, "serverRead", false);
}
//---------------------------------------------------------------------------
void __fastcall ServerThread::Execute()
{
	while (true)
	{
		if (serverRead->WaitFor(WAITING_DELAY) == wrSignaled)
		{
            serverRead->ResetEvent();
			BYTE * buffer;
			servBlockSize = Server->Socket->Connections[0]->ReceiveLength();
			buffer = new BYTE[servBlockSize];

			Server->Socket->Connections[0]->ReceiveBuf(buffer, servBlockSize);
			AnsiString parseBuf = (char *)buffer;
			if(parseBuf.SubString(1, parseBuf.Pos("@") - 1) == "FileName")//������ ������������ �������
			{
				isKey = false;
				servCurrentFileSize = 0;//��������� ����������� ������� ����� � 0
				parseBuf.Delete(1,parseBuf.Pos("@"));
				AnsiString dstFileNameAnsi = parseBuf.SubString(1, parseBuf.Pos("@") - 1);//������ ����� �����
				//�������������� AnsiString � wchar_t
				int size = dstFileNameAnsi.WideCharBufSize();
				servDstFileName = new WCHAR[size];
				dstFileNameAnsi.WideChar(servDstFileName, size);
				parseBuf.Delete(1, parseBuf.Pos("@"));
				servFileSize = StrToInt(parseBuf.SubString(1, parseBuf.Pos("@") - 1));//������ ������� �����
				parseBuf.Delete(1, parseBuf.Pos("@"));


				if(parseBuf.SubString(1,parseBuf.Pos("@") - 1) == "Key")//�������� �� �������� �� ������������ ���� ������
				{
					isKey = true;
				}
				else
				{
					ServerListBox->AddItem(L"��������� �����: " + dstFileNameAnsi.Delete(dstFileNameAnsi.Length() - 3, 4) + L" ...", NULL);
				}

				hServDst = CreateFileW(                 //��������� ���� ����������
							servDstFileName,
							GENERIC_WRITE,
							0,
							NULL,
							CREATE_ALWAYS,
							FILE_ATTRIBUTE_NORMAL,
							NULL);
			}
			else
			{
				DWORD nWrote;
				WriteFile(//������ � ����
					hServDst,
					buffer,
					servBlockSize,
					&nWrote,
					NULL);
				servCurrentFileSize += servBlockSize; //���������� ������� ���������� ���� �� ����� ���������� �� ������
				if (servCurrentFileSize >= servFileSize)
				{
					CloseHandle(hServDst); //�������� �����
					if(isKey)//���� ������ ����, �� ������������ ������������� �������������
					{
		//-----------------------------------------------------------------------------------------
		//                   ������������� ������������� (�� �������)
		//            �������� �����: ClientPublicKey.pub(����� ����) � PublicKey.pub(���� ����)
						servNetCrypt->Init(CryptoAlgorythms::MAGMA, CryptoAlgorythms::EXCHANGE_DH, L"ClientPublicKey.pub");
		//----------------------------------------------------------------------------------------
						DeleteFile(L"ClientPublicKey.pub");     //������� ��������� ���� ��������� ��������� ����� �������
						ServerListBox->AddItem(L"����������� ���������� ����������.", NULL);
					}
					else
					{
		//--------------------------------------------------------------------------------------------
		//              ����������� ����� servDstFileName � ��������� ���������� .enc

						String fileName = servDstFileName;
						fileName.Delete(fileName.Length() - 3, 4);
						servNetCrypt->DecryptFile(servDstFileName, fileName.c_str());
		//----------------------------------------------------------------------------------------------
						ServerListBox->AddItem(UnicodeString(L"���� ") + fileName.c_str() + L" �������� � �������� � �����������.", NULL);
						DeleteFile(servDstFileName);        //������� �������� ������������� ����
					}
				}
			}
			delete[] buffer;

		}
	}
}
//---------------------------------------------------------------------------
