﻿//---------------------------------------------------------------------------

#pragma hdrstop

#include "CryptoSystem.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

CryptoSystem::~CryptoSystem()
{
	// Уничтожение дескриптора сессионного ключа.
    if(hSessionKey)
        CryptDestroyKey(hSessionKey);

    // Освобождение дескриптора провайдера.
    if(hProv)
        CryptReleaseContext(hProv, 0);
}
