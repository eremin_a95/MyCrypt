//---------------------------------------------------------------------------

#ifndef ServerThreadH
#define ServerThreadH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <System.SyncObjs.hpp>
#include <System.Win.ScktComp.hpp>
#include <Vcl.StdCtrls.hpp>
#include "CryptoSystem.h"
//---------------------------------------------------------------------------
class ServerThread : public TThread
{
private:
	CryptoSystem * servNetCrypt;
	HANDLE hServDst;
	int servFileSize;
	int servCurrentFileSize;
	bool isKey;
	int servBlockSize;
	WCHAR * servDstFileName;
	TServerSocket * Server;
    TListBox * ServerListBox;
protected:
	void __fastcall Execute();
public:
	TEvent * serverRead;
	__fastcall ServerThread(CryptoSystem * servNetCrypt, TServerSocket * Server, TListBox * ServerListBox, bool CreateSuspended);
};
//---------------------------------------------------------------------------
#endif
