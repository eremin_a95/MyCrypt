//---------------------------------------------------------------------------

#ifndef CryptoProSystemH
#define CryptoProSystemH

#include "CryptoSystem.h"
//---------------------------------------------------------------------------
class CryptoProSystem : public CryptoSystem
{
private:
	BYTE * password;
    HCRYPTKEY hAgreeKey;

	ALG_ID SwitchAlg(int externalAlg);

    bool ExportPublicKey(HCRYPTKEY hKey, const WCHAR * fileName);

public:
	CryptoProSystem();
	virtual ~CryptoProSystem();

	bool Init(int alg, const BYTE * password);
	bool Init(int alg, const WCHAR * keyFile);
	bool Init(int symAlg, int keyExchangeAlg, const WCHAR * peerPublicKeyFileName);

	bool Encrypt(BYTE * buffer, bool final, DWORD * nEnc, int blockSize);

	bool Decrypt(BYTE * buffer, bool final, DWORD * nDec);

	bool EncryptFile(const WCHAR * fileName, const WCHAR * dstFileName);

	bool DecryptFile(const WCHAR * fileName, const WCHAR * dstFileName);

	bool ExportKey(const WCHAR * keyFileName);
	bool ExportKey(BYTE ** sessionKeyBlob, DWORD * sessionKeyBlobLength);
};






#endif
