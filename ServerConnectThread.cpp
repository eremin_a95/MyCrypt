//---------------------------------------------------------------------------

#include <System.hpp>
#pragma hdrstop

#include "ServerConnectThread.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------

//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(&UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall ServerConnectThread::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------
#define SLEEP_DURATION 300
#define WAITING_DELAY 10000


__fastcall ServerConnectThread::ServerConnectThread(TServerSocket * Server, bool CreateSuspended)
	: TThread(CreateSuspended)
{
	FreeOnTerminate = true;
	this->Server = Server;
	serverClientConnected = new TEvent(NULL, true, false, "serverClientConnected", false);
}
//---------------------------------------------------------------------------
void __fastcall ServerConnectThread::Execute()
{
	while (true)
	{
		if (serverClientConnected->WaitFor(WAITING_DELAY) == wrSignaled)
		{
			//�������� ��������� ����� �������
			WCHAR * srcFile = L"PublicKey.pub";
			TFileStream * myStream;
			myStream = new TFileStream(srcFile, fmOpenRead | fmShareDenyNone);
			Server->Socket->Connections[0]->SendText("FileName@ServerPublicKey.pub@" + IntToStr(myStream->Size)+ "@");
			Sleep(SLEEP_DURATION);
			Server->Socket->Connections[0]->SendStream(myStream);   //����� �������������, ��� ������� �� �����
			Sleep(SLEEP_DURATION * 2);
			//�������� ������������� ����������
			Server->Socket->Connections[0]->SendText("ConnectOK@");
			serverClientConnected->ResetEvent();
		}
	}
}
//---------------------------------------------------------------------------
