//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtCtrls.hpp>

//#include "CryptoSystem.h"
#include <Vcl.Tabs.hpp>
#include <Vcl.ComCtrls.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <System.Win.ScktComp.hpp>
#include <Vcl.Mask.hpp>
#include "ClientThread.h"
#include "ServerThread.h"
#include "ServerConnectThread.h"
//---------------------------------------------------------------------------
class TMainForm : public TForm
{
__published:	// IDE-managed Components
	TOpenDialog *SourceFileOpenDialog;
	TOpenDialog *KeyFileOpenDialog;
	TButton *OpenButton;
	TLabel *MainLabel;
	TSaveDialog *TargetFileSaveDialog;
	TRadioGroup *ModeRadioGroup;
	TButton *StartButton;
	TButton *OpenKeyButton;
	TComboBox *AlgComboBox;
	TGroupBox *ParamsGroupBox;
	TRadioGroup *UsingKeyRadioGroup;
	TEdit *PasswordEdit;
	TCheckBox *ExportKeyCheckBox;
	TButton *ExportKeyButton;
	TGroupBox *CryptoModeGroupBox;
	TSaveDialog *KeyFileSaveDialog;
	TLabel *PasswordLabel;
	TLabel *RepeatLabel;
	TEdit *RepeatEdit;
	TButton *OpenPublicKeyButton;
	TOpenDialog *PublicKeyFileOpenDialog;
	TGroupBox *NetGroupBox;
	TLabel *IPLabel;
	TButton *StartSessionButton;
	TButton *SendFileButton;
	TEdit *IPEdit;
	TOpenDialog *SendFileOpenDialog;
	TClientSocket *Client;
	TServerSocket *Server;
	TListBox *ServerListBox;
	TTabControl *SwitchTabControl;
	void __fastcall OpenButtonClick(TObject *Sender);
	void __fastcall OpenKeyButtonClick(TObject *Sender);
	void __fastcall StartButtonClick(TObject *Sender);
	void __fastcall ModeRadioGroupClick(TObject *Sender);
	void __fastcall UsingKeyRadioGroupClick(TObject *Sender);
	void __fastcall ExportKeyCheckBoxClick(TObject *Sender);
	void __fastcall ExportKeyButtonClick(TObject *Sender);
	void __fastcall RepeatEditChange(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall OpenPublicKeyButtonClick(TObject *Sender);
	void __fastcall StartSessionButtonClick(TObject *Sender);
	void __fastcall SendFileButtonClick(TObject *Sender);
	void __fastcall ClientDisconnect(TObject *Sender, TCustomWinSocket *Socket);
	void __fastcall ClientError(TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
	void __fastcall ClientRead(TObject *Sender, TCustomWinSocket *Socket);
	void __fastcall ServerClientDisconnect(TObject *Sender, TCustomWinSocket *Socket);
	void __fastcall ServerClientRead(TObject *Sender, TCustomWinSocket *Socket);
	void __fastcall ClientConnect(TObject *Sender, TCustomWinSocket *Socket);
	void __fastcall ServerListen(TObject *Sender, TCustomWinSocket *Socket);
	void __fastcall SwitchTabControlChange(TObject *Sender);
	void __fastcall ServerClientError(TObject *Sender, TCustomWinSocket *Socket, TErrorEvent ErrorEvent,
          int &ErrorCode);
	void __fastcall ServerClientConnect(TObject *Sender, TCustomWinSocket *Socket);



private:	// User declarations
	CryptoSystem * fileCrypt, * servNetCrypt,  * clientNetCrypt;
	HANDLE hServDst;
	int servFileSize;
	int servCurrentFileSize;
	bool isKey;
	int servBlockSize;
	WCHAR * servDstFileName;
	//TEvent * serverRead;
	ClientThread * clientThread;
	ServerThread * serverThread;
	ServerConnectThread * serverConnectThread;

    int SwitchAlg();
public:		// User declarations
	__fastcall TMainForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMainForm *MainForm;
//---------------------------------------------------------------------------
#endif
