//---------------------------------------------------------------------------

#ifndef CryptoProContainerH
#define CryptoProContainerH
#include "CryptoContainer.h"
#include <wincrypt.h>
//---------------------------------------------------------------------------
class CryptoProContainer : public CryptoContainer
{
private:
    HCERTSTORE hStore;

public:
	CryptoProContainer(const WCHAR * name = NULL);

	~CryptoProContainer();

	bool Open(const WCHAR * name);

	bool Create(const WCHAR * name);

	void Close();

	bool ExportPublicKey(const WCHAR * fileName);

	bool ExportPublicCert(const WCHAR * certName);

	bool ImportPublicCert(const WCHAR * certName);

	std::list<WCHAR *> GetCertList() const;

	BYTE * GetPublicKey(const WCHAR * certName);

};
#endif
